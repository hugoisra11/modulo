﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using System.Data;


namespace Connection
{
    class conexiongenerica
    {
        private DbCommand command;
        private DbConnection connection;
        private IDataReader read;

        public conexiongenerica(DbConnection connection)
        {
            this.connection = connection;
        }

        public void Open()
        {

            try
            {
                this.connection.Open();
                Console.WriteLine("Ud se ha conectado exitosamente");
            }
            catch { Console.WriteLine("Error de conexion"); }
        }

        public void Close()
        {
            this.connection.Close();

        }

        public  DataTable obtener_datos(string query)
        {
            this.Open();
            DataTable result = new DataTable();
            command = connection.CreateCommand();
            command.Connection = connection;
            command.CommandText = query;

            read = command.ExecuteReader();
            result.Load(read);
            read.Close();
            this.Close();
            return result;
                
        }
    }
}
